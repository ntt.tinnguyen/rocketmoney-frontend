import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListWalletComponent} from './components/list-wallet/list-wallet.component';
import {CreateWalletComponent} from './components/create-wallet/create-wallet.component';
import {CurrencyService} from './_services/currency.service';
import {WalletDetailComponent} from './components/wallet-detail/wallet-detail.component';
import {WalletService} from './_services/wallet.service';
import {ListWalletService} from './_services/list-wallet.service';
import {CheckReachSpendLimitService} from './_services/check-reach-spend-limit.service';
import {LogInComponent} from './components/log-in/log-in.component';
import {AuthGuardService} from './_auth/auth-guard.service';
import {UserInfoService} from './_services/user-info.service';

const routes: Routes = [
  {path: '', redirectTo: '/wallets', pathMatch: 'full'},
  {
    path: 'wallets',
    component: ListWalletComponent,
    resolve: {wallets: ListWalletService, user: UserInfoService},
    canActivate: [AuthGuardService]
  },
  {path: 'create-wallet', component: CreateWalletComponent, resolve: {currency: CurrencyService}, canActivate: [AuthGuardService]},
  {
    path: 'wallet/:id',
    component: WalletDetailComponent,
    resolve: {wallet: WalletService, reachSpendLimit: CheckReachSpendLimitService},
    canActivate: [AuthGuardService]
  },
  {path: 'login', component: LogInComponent},
  {path: '**', redirectTo: '/wallets'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
