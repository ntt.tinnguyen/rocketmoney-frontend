import {Component, OnInit} from '@angular/core';
import {CURRENCY} from '../../_constants/currency';
import {FormControl} from '@angular/forms';
import {Observable} from '../../../../node_modules/rxjs';
import {Currency} from '../../_models/currency';
import {map, startWith} from 'rxjs/operators';
import {CurrencyService} from '../../_services/currency.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Wallet} from '../../_models/wallet';
import {isNullOrUndefined} from 'util';
import {AlertService} from '../../_services/alert.service';
import {WalletService} from '../../_services/wallet.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-create-wallet',
  templateUrl: './create-wallet.component.html',
  styleUrls: ['./create-wallet.component.css']
})
export class CreateWalletComponent implements OnInit {
  public currencyList: Currency[];
  public wallet: Wallet;
  public isNameValid = true;
  public isAmountValid = true;

  constructor(private currencyService: CurrencyService,
              private activatedRoute: ActivatedRoute,
              private alertService: AlertService,
              private walletService: WalletService,
              private router: Router) {
    this.wallet = new Wallet();
    this.wallet.currencyCode = 'VND';
    this.wallet.amount = 0;
  }

  ngOnInit() {
    this.activatedRoute.data
      .subscribe((data: { currency: Currency[] }) => {
        this.currencyList = data.currency;
      }, err => {

      });
  }

  public createWallet() {
    this.checkFormValid();
    if (!this.isNameValid || !this.isAmountValid) {
      return;
    }
    this.walletService.createWallet(this.wallet).subscribe(
      wallet => {
        this.alertService.alertSuccess('Wallet has been created!');
        this.router.navigate(['wallet', wallet.id]);
      },
      error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }

  private checkFormValid() {
    this.isNameValid = !isNullOrUndefined(this.wallet.name) && this.wallet.name !== '';
    this.isAmountValid = !isNullOrUndefined(this.wallet.amount) && this.wallet.amount >= 0;
  }

  public onBack() {
    this.router.navigate(['list-wallet']);
  }
}
