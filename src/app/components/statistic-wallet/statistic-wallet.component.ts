import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataRange} from '../../_models/dataRange';
import {ChangeStatisticService} from '../../_services/change-statistic.service';
import {CategoryService} from '../../_services/category.service';
import {ChangeCategory} from '../../_models/change-category';

@Component({
  selector: 'app-statistic-wallet',
  templateUrl: './statistic-wallet.component.html',
  styleUrls: ['./statistic-wallet.component.css']
})
export class StatisticWalletComponent implements OnInit {
  public dataRange: DataRange;
  private walletId: string;
  public incomeStatistic = [];
  public spendingStatistic = [];
  public currencyCode: string;
  public categories: ChangeCategory[] = [];
  public inflow;
  public outflow;

  constructor(public dialogRef: MatDialogRef<StatisticWalletComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private changeStatisticService: ChangeStatisticService,
              private categoryService: CategoryService) {
    this.dataRange = this.data.dataRange;
    this.walletId = this.data.walletId;
    this.currencyCode = this.data.currencyCode;
    this.inflow = this.data.inflow;
    this.outflow = this.data.outflow;
  }

  ngOnInit() {
    this.getAllCategory();
    this.getStatisticSpendingChangesInRange();
    this.getStatisticIncomeChangesInRange();
  }

  public getThumbnail(name: string) {
    return this.categories.find(category => category.name === name).thumbnail;
  }

  private getAllCategory() {
    this.categoryService.getAllCategory().subscribe(
      categories => {
        this.categories = categories;
      }, error => {
      });
  }

  private getStatisticSpendingChangesInRange() {
    this.changeStatisticService.getStatisticSpendingChangesInRange(this.walletId, this.dataRange).subscribe(value => {
      this.spendingStatistic = value;
    });
  }

  private getStatisticIncomeChangesInRange() {
    this.changeStatisticService.getStatisticIncomeChangesInRange(this.walletId, this.dataRange).subscribe(value => {
      this.incomeStatistic = value;
    });
  }
}
