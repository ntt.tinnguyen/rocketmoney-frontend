import {Component, Input, OnInit} from '@angular/core';
import * as chartConfig from '../../../_constants/ngx-chart.config';
import {formatLabel} from '@swimlane/ngx-charts';

@Component({
  selector: 'app-ngx-pie-chart',
  templateUrl: './ngx-pie-chart.component.html',
  styleUrls: ['./ngx-pie-chart.component.css']
})
export class NgxPieChartComponent implements OnInit {
  @Input() public chart = [];
  public colorScheme = chartConfig.colorScheme.picnic;
  public animations = chartConfig.animations;
  public showLegend = chartConfig.showLegend;
  public legendTitle = chartConfig.legendTitle;
  public explodeSlices = chartConfig.explodeSlices;
  public showLabels = chartConfig.showLabels;
  public doughnut = chartConfig.doughnut;
  public arcWidth = chartConfig.arcWidth;
  public gradient = chartConfig.gradient;
  public tooltipDisabled = chartConfig.tooltipDisabled;
  public trimLabels = chartConfig.trimLabels;
  public view = [290, 180];

  constructor() {
  }

  ngOnInit() {
  }

  public onLegendLabelClick(entry) {
    console.log('Legend clicked', entry);
  }

  public pieTooltipText({data}) {
    const label = formatLabel(data.name);
    const val = formatLabel(data.value);
    return `
      <span class="tooltip-label">${label}</span>
      <span class="tooltip-val">${val}</span>
    `;
  }

  public dblclick(event) {
    console.log('Doube click', event);
  }

  select(data) {
    console.log('Item clicked', data);
  }
}
