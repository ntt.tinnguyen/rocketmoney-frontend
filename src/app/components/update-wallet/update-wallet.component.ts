import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {WalletService} from '../../_services/wallet.service';
import {isNullOrUndefined} from 'util';
import {Currency} from '../../_models/currency';
import {Wallet} from '../../_models/wallet';
import {CurrencyService} from '../../_services/currency.service';
import {AlertService} from '../../_services/alert.service';

@Component({
  selector: 'app-update-wallet',
  templateUrl: './update-wallet.component.html',
  styleUrls: ['./update-wallet.component.css']
})
export class UpdateWalletComponent implements OnInit {
  public currencyList: Currency[];
  public wallet: Wallet;
  public isNameValid = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<UpdateWalletComponent>,
              private walletService: WalletService,
              private currencyService: CurrencyService,
              private alertService: AlertService,
              public snackBar: MatSnackBar) {
    this.wallet = this.data.wallet;
  }

  ngOnInit() {
    this.getAllCurrency();
  }

  private getAllCurrency() {
    this.currencyService.getAllCurrency().subscribe(
      currencyList => {
        this.currencyList = currencyList;
      }, error => {
      });
  }

  private checkFormValid() {
    this.isNameValid = !isNullOrUndefined(this.wallet.name) && this.wallet.name !== '';
  }

  public onClose() {
    this.dialogRef.close();
  }

  public onUpdateBasicInfoWallet() {
    this.checkFormValid();
    if (!this.isNameValid) {
      return;
    }
    this.walletService.updateBasicInfoWallet(this.wallet).subscribe(
      data => {
        this.snackBar.open('Wallet has been updated', 'OK', {
          duration: 2000,
        });
        this.dialogRef.close();
      }, error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (404):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }
}
