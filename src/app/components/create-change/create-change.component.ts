import {Component, Inject, OnInit} from '@angular/core';
import {Change} from '../../_models/change';
import {CategoryService} from '../../_services/category.service';
import {ChangeCategory} from '../../_models/change-category';
import {ActivatedRoute, Router} from '@angular/router';
import {Wallet} from '../../_models/wallet';
import {isNullOrUndefined} from 'util';
import {ChangeService} from '../../_services/change.service';
import {AlertService} from '../../_services/alert.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-create-change',
  templateUrl: './create-change.component.html',
  styleUrls: ['./create-change.component.css']
})
export class CreateChangeComponent implements OnInit {

  public change: Change;
  public categories: ChangeCategory[] = [];
  public categoryError = false;
  public amountError = false;
  public changeTimeError = false;
  public currencyCode;
  private date = new Date();

  constructor(private changeService: ChangeService,
              private alertService: AlertService,
              public dialogRef: MatDialogRef<CreateChangeComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.change = new Change();
    this.change.changeTime = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate());
    this.currencyCode = this.data.currencyCode;
    this.categories = this.data.categories;
  }

  ngOnInit() {
    this.change.walletId = this.data.walletId;
  }

  public onCreateChange() {
    this.validateForm();
    if (this.categoryError || this.amountError || this.changeTimeError) {
      return;
    }
    this.changeService.createWalletChange(this.change).subscribe(
      value => {
        this.snackBar.open('Transaction has been created!', 'OK', {
          duration: 2000,
        });
        this.dialogRef.close();
      },
      error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }

  public validateForm() {
    this.amountError = isNullOrUndefined(this.change.amount) || this.change.amount === 0;
    this.categoryError = isNullOrUndefined(this.change.category);
    this.changeTimeError = isNullOrUndefined(this.change.changeTime);
  }

  public onClose() {
    this.dialogRef.close();
  }
}
