import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataRange} from '../../_models/dataRange';
import {ChangeService} from '../../_services/change.service';
import {Change} from '../../_models/change';
import {WalletService} from '../../_services/wallet.service';
import {Wallet} from '../../_models/wallet';
import {MatDialog} from '@angular/material';
import {StatisticWalletComponent} from '../statistic-wallet/statistic-wallet.component';
import {CreateChangeComponent} from '../create-change/create-change.component';
import {UpdateChangeComponent} from '../update-change/update-change.component';
import {CategoryService} from '../../_services/category.service';
import {ChangeCategory} from '../../_models/change-category';
import {AdjustBalanceComponent} from '../adjust-balance/adjust-balance.component';
import {UpdateWalletComponent} from '../update-wallet/update-wallet.component';
import {CHANGE_TYPE} from '../../_constants/change-type';
import {SpendLimitComponent} from '../spend-limit/spend-limit.component';
import {ReachSpendLimit} from '../../_models/reach-spend-limit';
import {SpendLimitService} from '../../_services/spend-limit.service';
import {AlertService} from '../../_services/alert.service';
import {CheckReachSpendLimitService} from '../../_services/check-reach-spend-limit.service';
import {SocketService} from '../../_services/socket.service';
import {UtilService} from '../../_services/util.service';

@Component({
  selector: 'app-wallet-detail',
  templateUrl: './wallet-detail.component.html',
  styleUrls: ['./wallet-detail.component.css']
})
export class WalletDetailComponent implements OnInit {
  public dataRange: DataRange;
  public changes: Change[] = [];
  public wallet: Wallet;
  private walletId;
  public range: Date[] = [];
  public change: Change;
  public inflow;
  public outflow;
  public sumFlow;
  public checkDialogOpen = false;
  private categories: ChangeCategory[] = [];
  public reachSpendLimit: ReachSpendLimit;
  private stompClient = null;

  constructor(private activeRoute: ActivatedRoute,
              private changeService: ChangeService,
              private walletService: WalletService,
              private activatedRoute: ActivatedRoute,
              private categoryService: CategoryService,
              private checkReachSpendLimitService: CheckReachSpendLimitService,
              private alertService: AlertService,
              private socketService: SocketService,
              private util: UtilService,
              public dialog: MatDialog) {
    this.dataRange = new DataRange();
    this.change = new Change();
  }

  ngOnInit() {
    this.activatedRoute.data
      .subscribe((data: { wallet: Wallet, reachSpendLimit: ReachSpendLimit}) => {
        this.wallet = data.wallet;
        this.reachSpendLimit = data.reachSpendLimit;
      }, err => {

      });
    this.walletId = this.activeRoute.snapshot.params['id'];
    this.changeDataRange(new Date());
    this.getWalletDetail(this.walletId, this.dataRange);
    this.getAllCategory();
    this.initSocket();
  }

  public getWalletDetail(walletId: string, dataRange: DataRange) {
    this.changeService.getWalletsDetailInRange(walletId, dataRange).subscribe(
      changes => {
        this.changes = changes;
        if (changes.length > 0) {
          this.calculateFlow(changes);
        }
      }, error => {
      });
  }

  public changeDataRange(date: Date) {
    this.range[1] = date;
    this.range[0] = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
    this.range[2] = new Date(date.getFullYear(), date.getMonth() + 1, date.getDate());
    this.dataRange.startDate = new Date(this.range[1].getFullYear(), this.range[1].getMonth(), 1);
    this.dataRange.endDate = new Date(this.range[1].getFullYear(), this.range[1].getMonth() + 1, 0);
    this.getWalletDetail(this.walletId, this.dataRange);
  }

  private calculateFlow(changes: Change[]) {
    this.inflow = changes.filter(change => change.category.changeType === 'INCOME')
      .reduce((sum, current) => sum + current.amount, 0);
    this.outflow = changes.filter(change => change.category.changeType === 'SPENDING')
      .reduce((sum, current) => sum + current.amount, 0);
    this.sumFlow = Math.abs(this.inflow - this.outflow);
  }

  public openStatisticDialog(): void {
    const dialogRef = this.dialog.open(StatisticWalletComponent, {
      id: 'statistic-dialog',
      maxWidth: '95vw',
      data: {
        dataRange: this.dataRange,
        walletId: this.walletId,
        currencyCode: this.wallet.currencyCode,
        inflow: this.inflow,
        outflow: this.outflow
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public openCreateChangeDialog(): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(CreateChangeComponent, {
        id: 'create-change-dialog',
        maxWidth: '95vw',
        data: {currencyCode: this.wallet.currencyCode, walletId: this.walletId, categories: this.categories}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getWalletDetail(this.walletId, this.dataRange);
        this.getWallet();
        this.checkReachSpendLimit();
        this.checkDialogOpen = false;
      });
    }
  }

  public openUpdateChangeDialog(change: Change): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(UpdateChangeComponent, {
        id: 'update-change-dialog',
        maxWidth: '95vw',
        data: {wallet: this.wallet, change: change, categories: this.categories}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getWalletDetail(this.walletId, this.dataRange);
        this.getWallet();
        this.checkReachSpendLimit();
        this.checkDialogOpen = false;
      });
    }
  }

  private getAllCategory() {
    this.categoryService.getAllCategory().subscribe(
      categories => {
        this.categories = categories;
      }, error => {
      });
  }

  private getWallet() {
    this.walletService.getWalletById(this.walletId).subscribe(wallet => this.wallet = wallet);
  }

  private checkReachSpendLimit() {
    this.checkReachSpendLimitService.checkReachSpendLimit(this.walletId).subscribe(
      value => {
        this.reachSpendLimit = value;
      }, error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (404):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }

  public openAdjustBalanceWalletDialog(): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(AdjustBalanceComponent, {
        id: 'adjust-balance-wallet-dialog',
        maxWidth: '95vw',
        data: {wallet: this.wallet}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getWalletDetail(this.walletId, this.dataRange);
        this.getWallet();
        this.checkDialogOpen = false;
      });
    }
  }

  public openUpdateBasicInfoWalletDialog(): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(UpdateWalletComponent, {
        id: 'update-wallet-dialog',
        maxWidth: '95vw',
        data: {wallet: this.wallet}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getWallet();
        this.checkDialogOpen = false;
      });
    }
  }

  public sumBlockAmount(changes: Change[]) {
    let sum = 0;
    changes.forEach(change => {
      if (change.category.changeType === CHANGE_TYPE.INCOME) {
        sum += change.amount;
      } else {
        sum -= change.amount;
      }
    });
    return sum;
  }

  public openSpendLimitDialog(): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(SpendLimitComponent, {
        id: 'create-spend-limit-dialog',
        maxWidth: '95vw',
        data: {wallet: this.wallet, reachSpendLimit: this.reachSpendLimit}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getWallet();
        this.checkReachSpendLimit();
        this.checkDialogOpen = false;
      });
    }
  }

  @HostListener('window:scroll', ['$event'])
  public  stickyHeader() {
    return window.pageYOffset > 50;
  }


  private initSocket() {
    this.stompClient = this.socketService.initializeWebSocketConnection();
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/ws-change/' + that.walletId, (change) => {
        if (change.body) {
          const walletChange = JSON.parse(change.body) as Change;
          const changeTime = UtilService.refactorDate(walletChange.changeTime);
          if (changeTime > that.range[0] && changeTime < that.range[2]) {
            that.getWalletDetail(that.walletId, that.dataRange);
            that.checkReachSpendLimit();
          }
        }
      });
    });
  }
}
