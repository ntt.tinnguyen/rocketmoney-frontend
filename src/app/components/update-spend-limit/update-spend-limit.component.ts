import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {SpendLimit} from '../../_models/spend-limit';
import {isNullOrUndefined} from 'util';
import {Wallet} from '../../_models/wallet';
import {SpendLimitService} from '../../_services/spend-limit.service';
import {AlertService} from '../../_services/alert.service';
import {UtilService} from '../../_services/util.service';

@Component({
  selector: 'app-update-spend-limit',
  templateUrl: './update-spend-limit.component.html',
  styleUrls: ['./update-spend-limit.component.css']
})
export class UpdateSpendLimitComponent implements OnInit {
  public spendLimit: SpendLimit;
  public wallet: Wallet;
  public amountError = false;
  public startDateError = false;
  public endDateError = false;
  public formError = false;

  constructor(public dialogRef: MatDialogRef<UpdateSpendLimitComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private spendLimitService: SpendLimitService,
              private alertService: AlertService) {
    this.wallet = this.data.wallet;
    this.spendLimit = this.data.spendLimit;
  }

  ngOnInit() {
    this.spendLimit.startDate = UtilService.refactorDate(this.spendLimit.startDate);
    this.spendLimit.endDate = UtilService.refactorDate(this.spendLimit.endDate);
  }

  public onUpdateSpendLimit() {
    this.checkFormValid();
    if (this.formError) {
      return;
    }
    this.spendLimitService.updateSpendLimit(this.spendLimit).subscribe(value => {
      this.snackBar.open('Spend Limit has been updated.', 'OK', {duration: 2000});
      this.dialogRef.close();
    }, error => {
      switch (error.status) {
        case (400):
          this.alertService.alertError(error.error.errorMessage);
          break;
        case (404):
          this.alertService.alertError(error.error.errorMessage);
          break;
        case (500):
          this.alertService.alertError(error.error.errorMessage);
          break;
        default:
          this.alertService.alertError('There are somethings was wrong.');
      }
    });
  }

  public onDeleteSpendLimit() {
    this.alertService.alertConfirmationDelete('This wallet will be deleted').then(result => {
      if (result.value) {
        this.spendLimitService.deleteSpendLimit(this.spendLimit.id).subscribe(
          value => {
            this.snackBar.open('Spend Limit has been deleted.', 'OK', {duration: 2000});
            this.dialogRef.close();
          }, error => {
            switch (error.status) {
              case (400):
                this.alertService.alertError(error.error.errorMessage);
                break;
              case (404):
                this.alertService.alertError(error.error.errorMessage);
                break;
              case (500):
                this.alertService.alertError(error.error.errorMessage);
                break;
              default:
                this.alertService.alertError('There are somethings was wrong.');
            }
          });
      }
    });
  }

  public checkStartDatValid() {
    this.startDateError = isNullOrUndefined(this.spendLimit.startDate);
  }

  public checkEndDateValid() {
    this.endDateError = isNullOrUndefined(this.spendLimit.endDate) || this.spendLimit.endDate <= this.spendLimit.startDate;
  }

  public checkAmountValid() {
    this.amountError = isNullOrUndefined(this.spendLimit.amount) || this.spendLimit.amount <= 0;
    console.log(this.amountError);
  }

  public onClose() {
    this.dialogRef.close();
  }

  private checkFormValid() {
    this.checkAmountValid();
    this.checkStartDatValid();
    this.checkEndDateValid();
    this.formError = this.amountError || this.startDateError || this.endDateError;
  }
}
