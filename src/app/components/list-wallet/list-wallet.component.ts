import {Component, OnInit} from '@angular/core';
import {WalletService} from '../../_services/wallet.service';
import {Wallet} from '../../_models/wallet';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatSnackBar} from '@angular/material';
import {UpdateWalletComponent} from '../update-wallet/update-wallet.component';
import {AdjustBalanceComponent} from '../adjust-balance/adjust-balance.component';
import {SocketService} from '../../_services/socket.service';
import {User} from '../../_models/user';
import {UserInfoService} from '../../_services/user-info.service';

@Component({
  selector: 'app-list-wallet',
  templateUrl: './list-wallet.component.html',
  styleUrls: ['./list-wallet.component.css']
})
export class ListWalletComponent implements OnInit {
  wallets: Wallet[] = [];
  public checkDialogOpen = false;
  private stompClient = null;
  public primaryWalletId = '';
  public user: User = new User();

  constructor(private walletService: WalletService,
              private alertService: AlertService,
              private activatedRoute: ActivatedRoute,
              public dialog: MatDialog,
              private router: Router,
              private socketService: SocketService,
              private userInfoService: UserInfoService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.activatedRoute.data
      .subscribe((data: { wallets: Wallet[], user: User }) => {
        this.user = data.user;
        this.wallets = data.wallets;
        this.alertService.closeAlert();
      }, err => {

      });
    this.initSocket();
    this.primaryWalletId = this.user.primaryWalletId;
  }

  private getAllWallets() {
    this.getUserInfo();
    this.walletService.getAllWallets().subscribe(
      wallets => {
        this.wallets = wallets;
      }, error => {
      });
  }

  public navigateToCreateWallet() {
    this.router.navigate(['create-wallet']);
  }

  public deleteWallet(walletId: string) {
    this.alertService.alertConfirmationDelete('This wallet will be deleted').then(result => {
      if (result.value) {
        this.walletService.deleteWalletById(walletId).subscribe(
          value => {
            this.alertService.alertSuccess('This wallet has been deleted.');
            this.getAllWallets();
          }, error => {

          });
      }
    });
  }

  public navigateToWalletDetail(walletId: string) {
    this.router.navigate(['wallet', walletId]).then();
  }

  public openUpdateBasicInfoWalletDialog(wallet: Wallet): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(UpdateWalletComponent, {
        id: 'update-wallet-dialog',
        maxWidth: '95vw',
        data: {wallet: wallet}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getAllWallets();
        this.checkDialogOpen = false;
      });
    }
  }

  public openAdjustBalanceWalletDialog(wallet: Wallet): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(AdjustBalanceComponent, {
        id: 'adjust-balance-wallet-dialog',
        maxWidth: '95vw',
        data: {wallet: wallet}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getAllWallets();
        this.checkDialogOpen = false;
      });
    }
  }

  private initSocket() {
    this.stompClient = this.socketService.initializeWebSocketConnection();
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/ws-wallets/' + that.user.email, (wallets) => {
        if (wallets.body) {
          that.wallets = JSON.parse(wallets.body) as Wallet [];
          that.getUserInfo();
        }
      });
    });
  }

  private getUserInfo() {
    this.userInfoService.getCurrentUserInfo().subscribe(user => {
      this.user = user;
      this.primaryWalletId = this.user.primaryWalletId;
    });
  }

  public setPrimaryWallet(walletId: string, walletName: string) {
    this.walletService.setPrimaryWallet(walletId).subscribe(
      value => {
        this.primaryWalletId = walletId;
        this.snackBar.open(walletName + ' now is your primary wallet', 'OK', {
          duration: 2000,
        });
      }, error => {

      });
  }
}
