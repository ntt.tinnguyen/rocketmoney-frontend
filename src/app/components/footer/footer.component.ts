import {Component, OnInit} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {LogInService} from '../../_services/log-in.service';
import {AuthService} from 'angular-6-social-login';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public loggedIn: boolean;

  constructor(private loginService: LogInService,
              private socialAuthService: AuthService) {
  }

  ngOnInit() {
    this.checkLogin();
  }

  private checkLogin() {
    this.socialAuthService.authState.subscribe(socialUser => {
      this.loggedIn = !isNullOrUndefined(socialUser);
    });
  }
}
