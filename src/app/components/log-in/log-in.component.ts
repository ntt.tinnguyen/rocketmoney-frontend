import {Component, OnInit} from '@angular/core';
import {LogInService} from '../../_services/log-in.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(private loginService: LogInService) {
  }

  ngOnInit() {
    this.checkLoggedIn();
  }

  public socialSignIn() {
    this.loginService.socialSignIn();
  }

  public checkLoggedIn() {
    this.loginService.checkLoggedIn();
  }

  public fakeSignIn() {
    this.loginService.fakeSignIn();
  }

}
