import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {isNullOrUndefined} from 'util';
import {LogInService} from '../../_services/log-in.service';
import {DOCUMENT} from '@angular/platform-browser';
import {User} from '../../_models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public facebookUser: User;

  constructor(private router: Router,
              private loginService: LogInService,
              @Inject(DOCUMENT) private document: any) {
  }

  ngOnInit() {
    this.setName();
  }

  public onBackHome() {
    this.router.navigate(['']);
  }

  private setName() {
    this.loginService.facebookUser.subscribe(facebookUser => {
      this.facebookUser = facebookUser;
    });
    if (isNullOrUndefined(this.facebookUser)) {
      const currentUser = localStorage.getItem('currentUser');
      if (!isNullOrUndefined(currentUser)) {
        this.loginService.facebookUser.next(JSON.parse(currentUser) as User);
      }
    }
  }

  public socialSignOut() {
    this.loginService.socialSignOut();
  }

  public navigateToRocketBot() {
    this.document.getElementById('navigateToRocketBot').click();
  }
}
