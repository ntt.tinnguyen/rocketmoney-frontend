import {Component, Inject, OnInit} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {WalletService} from '../../_services/wallet.service';
import {Wallet} from '../../_models/wallet';
import {AlertService} from '../../_services/alert.service';

@Component({
  selector: 'app-adjust-balance',
  templateUrl: './adjust-balance.component.html',
  styleUrls: ['./adjust-balance.component.css']
})
export class AdjustBalanceComponent implements OnInit {
  public amountError = false;
  public wallet: Wallet;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AdjustBalanceComponent>,
              private walletService: WalletService,
              private alertService: AlertService,
              public snackBar: MatSnackBar) {
    this.wallet = this.data.wallet;
  }

  ngOnInit() {
  }

  private checkFormValid() {
    this.amountError = isNullOrUndefined(this.wallet.amount);
  }

  public onClose() {
    this.dialogRef.close();
  }

  public onAdjustBalanceWallet() {
    this.checkFormValid();
    if (this.amountError) {
      return;
    }
    this.walletService.adjustWalletBalance(this.wallet).subscribe(
      data => {
        this.snackBar.open('Wallet balance has been adjusted', 'OK', {
          duration: 2000,
        });
        this.dialogRef.close();
      }, error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (404):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }
}
