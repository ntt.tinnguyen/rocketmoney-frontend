import {Component, Inject, OnInit} from '@angular/core';
import {ChangeService} from '../../_services/change.service';
import {AlertService} from '../../_services/alert.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {ChangeCategory} from '../../_models/change-category';
import {isNullOrUndefined} from 'util';
import {Change} from '../../_models/change';
import {Wallet} from '../../_models/wallet';
import {UtilService} from '../../_services/util.service';

@Component({
  selector: 'app-update-change',
  templateUrl: './update-change.component.html',
  styleUrls: ['./update-change.component.css']
})
export class UpdateChangeComponent implements OnInit {
  public categories: ChangeCategory[] = [];
  public categoryError = false;
  public amountError = false;
  public changeTimeError = false;
  public change: Change;
  public wallet: Wallet;
  public category: ChangeCategory;

  constructor(private changeService: ChangeService,
              private alertService: AlertService,
              public dialogRef: MatDialogRef<UpdateChangeComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.change = this.data.change;
    this.wallet = this.data.wallet;
    this.categories = this.data.categories;
    this.category = this.change.category;
  }

  ngOnInit() {
    this.change.changeTime = UtilService.refactorDate(this.change.changeTime);
  }

  public validateForm() {
    this.amountError = isNullOrUndefined(this.change.amount);
    this.categoryError = isNullOrUndefined(this.change.category);
    this.changeTimeError = isNullOrUndefined(this.change.changeTime);
  }

  public onUpdateChange() {
    this.validateForm();
    if (this.categoryError || this.amountError || this.changeTimeError) {
      return;
    }
    this.changeService.updateWalletChange(this.change).subscribe(
      value => {
        this.snackBar.open('Wallet change has been updated', 'OK', {
          duration: 2000,
        });
        this.dialogRef.close();
      }, error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (404):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }

  public onDeleteChange() {
    this.alertService.alertConfirmationDelete('This wallet will be deleted').then(result => {
      if (result.value) {
        this.changeService.deleteWalletChange(this.change.id).subscribe(
          value => {
            this.snackBar.open('Wallet change has been deleted', 'OK', {
              duration: 2000,
            });
            this.dialogRef.close();
          }, error => {
            switch (error.status) {
              case (400):
                this.alertService.alertError(error.error.errorMessage);
                break;
              case (404):
                this.alertService.alertError(error.error.errorMessage);
                break;
              case (500):
                this.alertService.alertError(error.error.errorMessage);
                break;
              default:
                this.alertService.alertError('There are somethings was wrong.');
            }
          });
      }
    });
  }

  public onClose() {
    this.dialogRef.close();
  }

  public compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }
}
