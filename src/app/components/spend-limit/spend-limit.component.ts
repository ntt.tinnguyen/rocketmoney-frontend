import {Component, Inject, OnInit} from '@angular/core';
import {SpendLimit} from '../../_models/spend-limit';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Wallet} from '../../_models/wallet';
import {SpendLimitService} from '../../_services/spend-limit.service';
import {isNullOrUndefined} from 'util';
import {UpdateSpendLimitComponent} from '../update-spend-limit/update-spend-limit.component';
import {AlertService} from '../../_services/alert.service';
import {ReachSpendLimit} from '../../_models/reach-spend-limit';
import {Change} from '../../_models/change';
import {SocketService} from '../../_services/socket.service';

@Component({
  selector: 'app-create-spend-limit',
  templateUrl: './spend-limit.component.html',
  styleUrls: ['./spend-limit.component.css']
})
export class SpendLimitComponent implements OnInit {
  spendLimit: SpendLimit;
  wallet: Wallet;
  spendLimits: SpendLimit[] = [];
  amountError = false;
  startDateError = false;
  endDateError = false;
  formError = false;
  checkDialogOpen = false;
  public reachSpendLimit: ReachSpendLimit;
  private stompClient = null;

  constructor(public dialogRef: MatDialogRef<SpendLimitComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private spendLimitService: SpendLimitService,
              private alertService: AlertService,
              public dialog: MatDialog,
              private socketService: SocketService) {
    this.spendLimit = new SpendLimit();
    this.wallet = this.data.wallet;
    this.reachSpendLimit = this.data.reachSpendLimit;
  }

  ngOnInit() {
    this.getAllSpendLimitsByWalletId(this.wallet.id);
    this.initSocket();
  }

  public onClose() {
    this.dialogRef.close();
  }

  public createSpendLimit() {
    this.checkFormValid();
    if (this.formError) {
      return;
    }
    this.spendLimit.walletId = this.wallet.id;
    this.spendLimitService.createSpendLimit(this.spendLimit).subscribe(
      value => {
        this.snackBar.open('Spend Limit has been created.', 'OK', {duration: 2000});
        this.spendLimit = new SpendLimit();
        this.getAllSpendLimitsByWalletId(this.wallet.id);
      }, error => {
        switch (error.status) {
          case (400):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (404):
            this.alertService.alertError(error.error.errorMessage);
            break;
          case (500):
            this.alertService.alertError(error.error.errorMessage);
            break;
          default:
            this.alertService.alertError('There are somethings was wrong.');
        }
      });
  }

  public checkStartDatValid() {
    this.startDateError = isNullOrUndefined(this.spendLimit.startDate);
  }

  public checkEndDateValid() {
    this.endDateError = isNullOrUndefined(this.spendLimit.endDate) || this.spendLimit.endDate <= this.spendLimit.startDate;
  }

  public checkAmountValid() {
    this.amountError = isNullOrUndefined(this.spendLimit.amount) || this.spendLimit.amount <= 0;
  }

  public openUpdateSpendLimitDialog(spendLimit: SpendLimit): void {
    if (!this.checkDialogOpen) {
      this.checkDialogOpen = true;
      const dialogRef = this.dialog.open(UpdateSpendLimitComponent, {
        id: 'update-spend-limit-dialog',
        maxWidth: '95vw',
        data: {spendLimit: spendLimit, wallet: this.wallet}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getAllSpendLimitsByWalletId(this.wallet.id);
        this.checkDialogOpen = false;
      });
    }
  }

  public getSpendLimitProgress(spendLimit: SpendLimit) {
    const currentSpent = this.reachSpendLimit.spendLimitAssistants.find(
      spendLimitAssistant => spendLimitAssistant.spendLimitId === spendLimit.id);
    if (isNullOrUndefined(currentSpent)) {
      return 0;
    }
    const progress = currentSpent.totalCurrentSpent / spendLimit.amount * 100;
    return progress > 100 ? 100 : progress;
  }

  private checkFormValid() {
    this.checkAmountValid();
    this.checkStartDatValid();
    this.checkEndDateValid();
    this.formError = this.amountError || this.startDateError || this.endDateError;
  }

  private getAllSpendLimitsByWalletId(walletId: string) {
    this.spendLimitService.getAllSpendLimitByWalletId(walletId).subscribe(spendLimits => {
      this.spendLimits = spendLimits;
    }, error => {
      switch (error.status) {
        case (400):
          this.alertService.alertError(error.error.errorMessage);
          break;
        case (404):
          this.alertService.alertError(error.error.errorMessage);
          break;
        case (500):
          this.alertService.alertError(error.error.errorMessage);
          break;
        default:
          this.alertService.alertError('There are somethings was wrong.');
      }
    });
  }

  private initSocket() {
    this.stompClient = this.socketService.initializeWebSocketConnection();
    const that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/ws-spendlimit/' + that.wallet.id, (spendLimit) => {
        if (spendLimit.body) {
          that.getAllSpendLimitsByWalletId(that.wallet.id);
        }
      });
    });
  }
}
