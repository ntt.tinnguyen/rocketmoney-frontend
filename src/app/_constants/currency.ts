export const CURRENCY = {
  VND: {code: 'VND', name: 'Vietnam Dong'},
  USD: {code: 'USD', name: 'United States dollar'}
};
