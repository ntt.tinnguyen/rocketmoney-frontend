/**
 * Base URL
 * */
// export const BASE_BACKEND_URL = 'http://localhost:9090';
export const BASE_BACKEND_URL = 'https://rocketmoney.cf';

export const BACKEND_URL = BASE_BACKEND_URL + '/api';
export const BACKEND_SOCKET_URL = BASE_BACKEND_URL + '/socket';

export const BASE_WALLET_URL = BACKEND_URL + '/wallets';
export const BASE_CURRENCY_WALLET_URL = BACKEND_URL + '/currency';
export const BASE_CHANGES_URL = BACKEND_URL + '/changes';
export const BASE_CATEGORY_URL = BACKEND_URL + '/categories';
export const BASE_SPENDLIMIT_URL = BACKEND_URL + '/spend-limits';
export const BASE_USER_URL = BACKEND_URL + '/users';

export const SOCKET_URL = BASE_BACKEND_URL + '/socket-wallets';

export const WALLET_URL = {
  WALLET_AMOUNT_URL: BASE_WALLET_URL + '/amount',
  WALLET_BASIC_INFO_URL: BASE_WALLET_URL + '/basic-info',
  WALLET_ADJUST_BALANCE_URL: BASE_WALLET_URL + '/adjust-balance',
  SET_PRIMARY_WALLET: BASE_WALLET_URL + '/primary'
};

export const CHANGES_URL = {
  CHANGES_IN_RANGE_URL: BASE_CHANGES_URL + '/range',
  INCOME_CHANGES_STATISTIC_URL: BASE_CHANGES_URL + '/statistic-income',
  SPENDING_CHANGES_STATISTIC_URL: BASE_CHANGES_URL + '/statistic-spending',
};

export const SPEND_LIMIT_URL = {
  SPEND_LIMITS_BY_WALLET: BASE_SPENDLIMIT_URL + '/wallet',
  CHECK_REACH_SPEND_LIMITS: BASE_SPENDLIMIT_URL + '/reach-limit',
};

export const USER_URL = {
  USER_INFO_BY_EMAIL: BASE_USER_URL + '/info'
};
