export const colorScheme = {
  cool: {
    name: 'cool',
    selectable: true,
    group: 'Ordinal',
    domain: [
      '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886'
    ]
  },
  picnic: {
    name: 'picnic',
    selectable: false,
    group: 'Ordinal',
    domain: [
      '#FAC51D', '#66BD6D', '#FAA026', '#29BB9C', '#E96B56', '#55ACD2', '#B7332F', '#2C83C9', '#9166B8', '#92E7E8'
    ]
  }
};
export const animations = true;
export const showLegend = false;
export const legendTitle = 'Legend';
export const explodeSlices = false;
export const showLabels = true;
export const doughnut = true;
export const arcWidth = 0.40;
export const gradient = false;
export const tooltipDisabled = false;
export const trimLabels = false;
export const view = [430, 400];
