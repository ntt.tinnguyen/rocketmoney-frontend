import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {BASE_CURRENCY_WALLET_URL} from '../_constants/path';
import {Observable, of} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Currency} from '../_models/currency';
import {AlertService} from './alert.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class CurrencyService implements Resolve<any>  {
  public hasError = false;

  constructor(private httpService: HttpService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | boolean | Currency {
    return this.getAllCurrency().pipe(
      catchError(err => {
        this.hasError = true;
        return of(err);
      })
    );
  }

  public getAllCurrency(): Observable<any> {
    return this.httpService.get(BASE_CURRENCY_WALLET_URL);
  }
}
