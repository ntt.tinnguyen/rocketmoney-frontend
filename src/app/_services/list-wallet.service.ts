import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot} from '@angular/router';
import {Wallet} from '../_models/wallet';
import {catchError} from 'rxjs/operators';
import {WalletService} from './wallet.service';
import {Observable, of} from '../../../node_modules/rxjs';
import {AlertService} from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class ListWalletService {

  constructor(private walletService: WalletService,
              private alertService: AlertService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | boolean | Wallet {
    this.alertService.alertLoading('Loading...');
    return this.walletService.getAllWallets().pipe(
      catchError(err => {
        return of(err);
      })
    );
  }
}
