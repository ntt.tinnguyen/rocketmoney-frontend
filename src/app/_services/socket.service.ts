import { Injectable } from '@angular/core';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {BACKEND_SOCKET_URL} from '../_constants/path';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private stompClient = null;
  constructor() { }

  public initializeWebSocketConnection() {
    const ws = new SockJS(BACKEND_SOCKET_URL);
    this.stompClient = Stomp.over(ws);
    const that = this;
    return this.stompClient;
  }
}
