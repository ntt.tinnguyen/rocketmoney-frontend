import { Injectable } from '@angular/core';
import {BASE_CHANGES_URL, CHANGES_URL} from '../_constants/path';
import {Observable} from '../../../node_modules/rxjs';
import {HttpService} from './http.service';
import {DataRange} from '../_models/dataRange';
import {Change} from '../_models/change';

@Injectable({
  providedIn: 'root'
})
export class ChangeService {

  constructor(private httpService: HttpService) { }

  public getWalletsDetailInRange(walletId: string, dataRange: DataRange): Observable<any> {
    return this.httpService.post(CHANGES_URL.CHANGES_IN_RANGE_URL + '/' + walletId, dataRange);
  }

  public createWalletChange(change: Change): Observable<any> {
    return this.httpService.post(BASE_CHANGES_URL, change);
  }

  public updateWalletChange(change: Change): Observable<any> {
    return this.httpService.put(BASE_CHANGES_URL, change);
  }

  public deleteWalletChange(changeId: string): Observable<any> {
    return this.httpService.delete(BASE_CHANGES_URL + '/' + changeId);
  }
}
