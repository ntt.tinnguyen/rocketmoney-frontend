import {Injectable} from '@angular/core';
import {SPEND_LIMIT_URL} from '../_constants/path';
import {HttpService} from './http.service';
import {Observable, of} from '../../../node_modules/rxjs';
import {ActivatedRouteSnapshot} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {ReachSpendLimit} from '../_models/reach-spend-limit';

@Injectable({
  providedIn: 'root'
})
export class CheckReachSpendLimitService {

  constructor(private httpService: HttpService) {
  }

  public checkReachSpendLimit(walletId: string): Observable<any> {
    return this.httpService.get(SPEND_LIMIT_URL.CHECK_REACH_SPEND_LIMITS + '/' + walletId);
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | boolean | ReachSpendLimit {
    return this.checkReachSpendLimit(route.params['id']).pipe(
      catchError(err => {
        return of(err);
      })
    );
  }
}
