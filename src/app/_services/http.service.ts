import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FacebookUser} from '../_models/facebook-user';

const httpOptions = {
  headers: new HttpHeaders(
    {'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  public getWithParam(url, param) {
    return this.http.get(url, {params: param});
  }

  public getWithHeader(url, headers) {
    return this.http.get(url, {headers: headers});
  }

  public postWithHeader(url, data, headers) {
    return this.http.post(url, data, {headers: headers});
  }

  public putWithHeader(url, data, headers) {
    return this.http.put(url, data, {headers: headers});
  }

  public deleteWithHeader(url, headers) {
    return this.http.delete(url, {headers: headers});
  }

  public get(url) {
    return this.http.get(url);
  }

  public post(url, data) {
    return this.http.post(url, data);
  }

  public put(url, data) {
    return this.http.put(url, data);
  }

  public delete(url) {
    return this.http.delete(url);
  }
}
