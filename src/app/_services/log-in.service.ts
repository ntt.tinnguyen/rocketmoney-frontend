import {Injectable} from '@angular/core';
import {AuthService, FacebookLoginProvider} from 'angular-6-social-login';
import {Router} from '@angular/router';
import {FacebookUser} from '../_models/facebook-user';
import {Observable, Subject} from '../../../node_modules/rxjs';
import {BASE_USER_URL} from '../_constants/path';
import {HttpService} from './http.service';
import {UtilService} from './util.service';
import {User} from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class LogInService {
  public facebookUser: Subject<User>;

  constructor(private socialAuthService: AuthService,
              private router: Router,
              private httpService: HttpService,
              private utilService: UtilService) {
    this.facebookUser = new Subject();
  }

  public socialSignIn() {
    const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        const facebookUser: FacebookUser = JSON.parse(JSON.stringify(userData)) as FacebookUser;
        this.userWebLoginProcess(facebookUser).subscribe(user => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.facebookUser.next(user);
          this.router.navigate(['/wallets']);
        });
      }
    );
  }

  public socialSignOut() {
    this.socialAuthService.signOut().then(() => {
        this.facebookUser.next(null);
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
      }
    );
  }

  public checkLoggedIn() {
    if (localStorage.getItem('currentUser')) {
      this.router.navigate(['/wallets']);
    }
  }

  private userWebLoginProcess(facebookUser: FacebookUser): Observable<any> {
    const user: User = this.utilService.parseFacebookUserToUser(facebookUser);
    return this.httpService.post(BASE_USER_URL, user);
  }

  public fakeSignIn() {
    const userData = {
      email: 'upkey1010@gmail.com',
      name: 'Tín Nguyễn'
    };
    localStorage.setItem('currentUser', JSON.stringify(userData));
    const facebookUser: FacebookUser = JSON.parse(localStorage.getItem('currentUser')) as FacebookUser;
    this.facebookUser.next(this.utilService.parseFacebookUserToUser(facebookUser));
    this.userWebLoginProcess(facebookUser).subscribe(value => {
      this.router.navigate(['/wallets']);
    });
  }
}
