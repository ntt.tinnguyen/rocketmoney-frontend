import {Injectable} from '@angular/core';
import {FacebookUser} from '../_models/facebook-user';
import {User} from '../_models/user';
import {HttpHeaders} from '../../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() {
  }

  static refactorDate(date: Date) {
    const refactoredDate = new Date();
    const dateInfo = date.toString().substring(0, 10).split('-');
    refactoredDate.setDate(+dateInfo[2] + 1);
    refactoredDate.setMonth(+dateInfo[1] - 1);
    refactoredDate.setFullYear(+dateInfo[0]);
    return refactoredDate;
  }

  static returnEmailHeaders() {
    const currentUser: FacebookUser = JSON.parse(localStorage.getItem('currentUser')) as FacebookUser;
    return new HttpHeaders({'email': currentUser.email});
  }

  public parseFacebookUserToUser(facebookUser: FacebookUser) {
    const user = new User();
    user.name = facebookUser.name;
    user.email = facebookUser.email;
    user.facebookId = facebookUser.id;
    user.profilePicture = facebookUser.image;
    user.token = facebookUser.token;
    return user;
  }
}
