import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable, of} from '../../../node_modules/rxjs';
import {BASE_WALLET_URL, USER_URL, WALLET_URL} from '../_constants/path';
import {Wallet} from '../_models/wallet';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {FacebookUser} from '../_models/facebook-user';
import {HttpHeaders} from '@angular/common/http';
import {UtilService} from './util.service';

@Injectable({
  providedIn: 'root'
})
export class WalletService implements Resolve<any> {

  constructor(private httpService: HttpService) {

  }


  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | boolean | Wallet {
    const id = route.params['id'];
    return this.getWalletById(id).pipe(
      catchError(err => {
        return of(err);
      })
    );
  }

  public getAllWallets(): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.getWithHeader(BASE_WALLET_URL, headers);
  }

  public deleteWalletById(walletId: string): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.deleteWithHeader(BASE_WALLET_URL + '/' + walletId, headers);
  }

  public createWallet(wallet: Wallet): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.postWithHeader(BASE_WALLET_URL, wallet, headers);
  }

  public getWalletById(walletId: string): Observable<any> {
    return this.httpService.get(BASE_WALLET_URL + '/' + walletId);
  }

  public updateBasicInfoWallet(wallet: Wallet): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.putWithHeader(WALLET_URL.WALLET_BASIC_INFO_URL, wallet, headers);
  }

  public adjustWalletBalance(wallet: Wallet): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.putWithHeader(WALLET_URL.WALLET_ADJUST_BALANCE_URL, wallet, headers);
  }

  public setPrimaryWallet(walletId: string): Observable<any> {
    const headers = UtilService.returnEmailHeaders();
    return this.httpService.getWithHeader(WALLET_URL.SET_PRIMARY_WALLET + '/' + walletId, headers);
  }
}
