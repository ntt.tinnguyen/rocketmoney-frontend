import {Injectable} from '@angular/core';
import {DataRange} from '../_models/dataRange';
import {CHANGES_URL} from '../_constants/path';
import {HttpService} from './http.service';
import {Observable} from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChangeStatisticService {

  constructor(private httpService: HttpService) {
  }

  public getStatisticSpendingChangesInRange(walletId: string, dataRange: DataRange): Observable<any> {
    return this.httpService.post(CHANGES_URL.SPENDING_CHANGES_STATISTIC_URL + '/' + walletId, dataRange);
  }

  public getStatisticIncomeChangesInRange(walletId: string, dataRange: DataRange): Observable<any> {
    return this.httpService.post(CHANGES_URL.INCOME_CHANGES_STATISTIC_URL + '/' + walletId, dataRange);
  }
}
