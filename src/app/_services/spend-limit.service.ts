import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {BASE_SPENDLIMIT_URL, SPEND_LIMIT_URL} from '../_constants/path';
import {SpendLimit} from '../_models/spend-limit';

@Injectable({
  providedIn: 'root'
})
export class SpendLimitService {

  constructor(private httpService: HttpService) {
  }

  public getAllSpendLimitByWalletId(walletId: string): Observable<any> {
    return this.httpService.get(SPEND_LIMIT_URL.SPEND_LIMITS_BY_WALLET + '/' + walletId);
  }

  public createSpendLimit(spendLimit: SpendLimit): Observable<any> {
    return this.httpService.post(BASE_SPENDLIMIT_URL, spendLimit);
  }

  public deleteSpendLimit(id: string): Observable<any> {
    return this.httpService.delete(BASE_SPENDLIMIT_URL + '/' + id);
  }

  public updateSpendLimit(spendLimit: SpendLimit): Observable<any> {
    return this.httpService.put(BASE_SPENDLIMIT_URL, spendLimit);
  }
}
