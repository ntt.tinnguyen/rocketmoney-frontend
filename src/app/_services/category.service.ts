import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {BASE_CATEGORY_URL} from '../_constants/path';
import {Observable} from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpService: HttpService) {
  }

  public getAllCategory(): Observable<any> {
    return this.httpService.get(BASE_CATEGORY_URL);
  }
}
