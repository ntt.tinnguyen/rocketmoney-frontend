import { Injectable } from '@angular/core';
import {USER_URL} from '../_constants/path';
import {Observable, of} from '../../../node_modules/rxjs';
import {HttpService} from './http.service';
import {ActivatedRouteSnapshot} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {FacebookUser} from '../_models/facebook-user';
import {User} from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  constructor(private httpService: HttpService) { }

  public getCurrentUserInfo(): Observable<any> {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) as FacebookUser;
    return this.httpService.get(USER_URL.USER_INFO_BY_EMAIL + '/' + currentUser.email);
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | boolean | User {
    return this.getCurrentUserInfo().pipe(
      catchError(err => {
        return of(err);
      })
    );
  }
}
