export class User {
  public id: string;
  public createdAt: Date;
  public updatedAt: Date;
  public name: string;
  public email: string;
  public messengerSenderId: string;
  public facebookId: string;
  public profilePicture: string;
  public token: string;
  public primaryWalletId: string;
}
