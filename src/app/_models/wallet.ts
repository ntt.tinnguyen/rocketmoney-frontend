import {Change} from './change';

export class Wallet {
  public id: string;
  public name: string;
  public currencyCode: string;
  public amount: number;
  public owner: string;
  public changes: Change[];
  public createdAt: Date;
  public updatedAt: Date;
  public primaryWallet: boolean;
}
