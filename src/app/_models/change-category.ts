export class ChangeCategory {
  public id: string;
  public name: string;
  public changeType: string;
  public deletable: boolean;
  public thumbnail: string;
  public createdAt: Date;
  public updatedAt: Date;
}
