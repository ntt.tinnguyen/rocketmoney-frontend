export class FacebookUser {
  public id: string;
  public name: string;
  public email: string;
  public image: string;
  public token: string;
}
