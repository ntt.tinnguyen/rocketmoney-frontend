export class SpendLimit {
  public id: string;
  public name: string;
  public walletId: string;
  public amount: number;
  public speed: number;
  public startDate: Date;
  public endDate: Date;
  public createdAt: Date;
  public updatedAt: Date;
}
