import {SpendLimitAssistant} from './spend-limit-assistant';

export class ReachSpendLimit {
  public reachLimit: boolean;
  public spendLimitAssistants: SpendLimitAssistant[];
}
