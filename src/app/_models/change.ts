import {ChangeCategory} from './change-category';

export class Change {
  public id: string;
  public name: string;
  public amount: number;
  public note: string;
  public withWhom: string;
  public category: ChangeCategory;
  public changeTime: Date;
  public walletId: string;
  public createdAt: Date;
  public updatedAt: Date;
}
