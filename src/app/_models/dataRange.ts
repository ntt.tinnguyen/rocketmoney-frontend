export class DataRange {
  public startDate: Date;
  public endDate: Date;
}
