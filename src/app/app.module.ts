import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ListWalletComponent} from './components/list-wallet/list-wallet.component';
import {AppRoutingModule} from './app-routing.module';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {CreateWalletComponent} from './components/create-wallet/create-wallet.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatIconModule,
  MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
  MatOptionModule, MatRadioModule,
  MatSelectModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatTableModule, MatTabsModule, MatTooltipModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyService} from './_services/currency.service';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {WalletDetailComponent} from './components/wallet-detail/wallet-detail.component';
import {GroupByPipe} from './custom-pipe/groupBy.pipe';
import {WalletService} from './_services/wallet.service';
import {CreateChangeComponent} from './components/create-change/create-change.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NgxPieChartComponent} from './components/statistic-wallet/ngx-pie-chart/ngx-pie-chart.component';
import {StatisticWalletComponent} from './components/statistic-wallet/statistic-wallet.component';
import {UpdateChangeComponent} from './components/update-change/update-change.component';
import {UpdateWalletComponent} from './components/update-wallet/update-wallet.component';
import {AdjustBalanceComponent} from './components/adjust-balance/adjust-balance.component';
import {SpendLimitComponent} from './components/spend-limit/spend-limit.component';
import {UpdateSpendLimitComponent} from './components/update-spend-limit/update-spend-limit.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {ClickStopPropagationDirective} from './directive/click-stop-propagation.directive';
import {
  SocialLoginModule,
  AuthServiceConfig,
  FacebookLoginProvider,
} from 'angular-6-social-login';
import { LogInComponent } from './components/log-in/log-in.component';
import {StorageServiceModule} from 'angular-webstorage-service';

const material_module = [
  BrowserAnimationsModule,
  MatInputModule,
  MatButtonModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatOptionModule,
  MatSelectModule,
  MatIconModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatSortModule,
  MatTableModule,
  MatTableModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatRadioModule,
  MatDialogModule,
  MatExpansionModule,
  MatListModule,
  MatTabsModule,
  MatSnackBarModule,
  MatTooltipModule,
  MatProgressBarModule
];

export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('2080540985331260')
      }
    ]
  );
}

@NgModule({
  declarations: [
    AppComponent,
    ListWalletComponent,
    HeaderComponent,
    FooterComponent,
    CreateWalletComponent,
    WalletDetailComponent,
    GroupByPipe,
    CreateChangeComponent,
    NgxPieChartComponent,
    StatisticWalletComponent,
    UpdateChangeComponent,
    UpdateWalletComponent,
    AdjustBalanceComponent,
    SpendLimitComponent,
    UpdateSpendLimitComponent,
    ClickStopPropagationDirective,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    material_module,
    CurrencyMaskModule,
    NgxChartsModule,
    SocialLoginModule,
    StorageServiceModule
  ],
  entryComponents: [
    StatisticWalletComponent,
    CreateChangeComponent,
    UpdateChangeComponent,
    UpdateWalletComponent,
    AdjustBalanceComponent,
    SpendLimitComponent,
    UpdateSpendLimitComponent
  ],
  providers: [
    CurrencyService,
    WalletService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
